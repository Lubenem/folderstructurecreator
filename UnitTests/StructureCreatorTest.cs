using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Xunit;

namespace UnitTests
{
    public class StructureCreatorTest
    {
        [Fact]
        public void CreateStructure()
        {
            string testSourceFolder = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments)
            + Path.DirectorySeparatorChar + "FolderStructure";
            string testPatternString = @"(Folder Name[Nested Folder Name, Another Nested Folder]),
                                         (Another Folder Name [Nested Folder Name])";

            StructureCreator.CreateStructure(testSourceFolder, testPatternString);

            var expectedDict = new Dictionary<string, string[]>
            {
                {"Folder Name", new string[] {"Nested Folder Name","Another Nested Folder"}},
                {"Another Folder Name", new string[] {"Nested Folder Name"}}
            };

            var testDict = new Dictionary<string, string[]>();
            var dirPaths = Directory.GetDirectories(testSourceFolder);
            foreach (var dirPath in dirPaths)
            {
                var subdirPaths = Directory.GetDirectories(dirPath);
                var key = Path.GetFileName(dirPath);
                var value = subdirPaths.Select(p => Path.GetFileName(p)).ToArray();
                testDict.Add(key, value);
            }

            Assert.Equal(expectedDict, testDict);
        }
    }
}
