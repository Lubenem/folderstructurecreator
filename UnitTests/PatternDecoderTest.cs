using System.Collections.Generic;
using Xunit;

namespace UnitTests
{
    public class PatternDecoderTest
    {
        [Fact]
        public void GetStringsBetweenTest()
        {
            string testText = "(item), (item1), (item2)";
            string[] testDelimiters = new string[] { "(", ")" };
            List<string> testResult = PatternDecoder.GetStringsBetween(testText, testDelimiters);

            var expectedResult = new List<string> { "item", "item1", "item2" };

            Assert.Equal(expectedResult, testResult);
        }

        [Fact]
        public void GetFoldersTest()
        {
            string testPattern = @"(Folder Name[Nested Folder Name, Another Nested Folder]),
                                 (Another Folder Name [Nested Folder Name])";
            Dictionary<string, string[]> testResult = PatternDecoder.GetFolders(testPattern);

            var expectedResult = new Dictionary<string, string[]>
            {
                {"Folder Name", new string[] {"Nested Folder Name","Another Nested Folder"}},
                {"Another Folder Name", new string[] {"Nested Folder Name"}}
            };

            Assert.Equal(expectedResult, testResult);
        }
    }
}
