using System;
using System.Collections.Generic;
using System.Linq;

public class PatternDecoder
{
    public static Dictionary<string, string[]> GetFolders(string pattern)
    {
        List<string> folderList = GetStringsBetween(pattern, new string[] { "(", ")" });

        var folderDict = new Dictionary<string, string[]>();

        foreach (string item in folderList)
        {
            List<string> nestedFolders = GetStringsBetween(item, new string[] { "[", "]" });

            string folderName = nestedFolders[0].Trim();
            string[] nestedFolderNames = nestedFolders[1].Split(',').Select(p => p.Trim()).ToArray();

            folderDict.Add(folderName, nestedFolderNames);
        }

        return folderDict;
    }

    public static List<string> GetStringsBetween(string text, string[] delimiters)
    {
        var result = new List<string>();

        var strings = text.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);

        foreach (string item in strings)
        {
            if (!String.IsNullOrWhiteSpace(item) && item.Trim() != ",")
                result.Add(item);
        }

        return result;
    }    
}