using System.Collections.Generic;
using System.IO;

public class StructureCreator
{
    public static void CreateStructure(string sourceFolder, string pattern)
    {
        Dictionary<string, string[]> data = PatternDecoder.GetFolders(pattern);
        char dirSeparator = Path.DirectorySeparatorChar;
        foreach (KeyValuePair<string, string[]> entry in data)
            foreach (string nestedFolder in entry.Value)
                Directory.CreateDirectory(sourceFolder + dirSeparator + entry.Key + dirSeparator + nestedFolder);
    }
}