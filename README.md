# FolderStructureCreator

Create a piece of software that will:

    - take some "source" folder and pattern as an input parameter
    - will create a folder structure based on pattern

Pattern sample:
(Folder Name[Nester Folder Name, Another Nested Folder]), (Another Folder Name [Nester Folder Name])
